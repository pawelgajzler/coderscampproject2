function createUnitContainer(unitSymbol)
{
    return { 
        "value": 0, 
        "symbol": unitSymbol 
    };
}

function getEnteredValue()
{
    return parseFloat(document.getElementById("inputTemperature").value).toFixed(2);
}

document.getElementById("convert").addEventListener("click", convertScript);

function convertScript()
{ 
    let temperature = parseFloat(document.getElementById("inputTemperature").value);
    let scale = document.getElementById("option");
    let i = scale.selectedIndex;
    
    let userTemp = document.getElementById("userTemp");
    
    let display = {
        "celsius": createUnitContainer(" &#8451"),
        "kelvin": createUnitContainer(" &#8490"),
        "fahrenheit": createUnitContainer(" &#8457"),
        "rankine": createUnitContainer(" &#176R")
    };
    

    if (temperature === NaN || document.getElementById("inputTemperature").value.length == 0) 
    {
        document.getElementById("inputTemperature").className = "data__input data__input--error";
        userTemp.innerHTML = "<b>Podana wartość nie jest liczbą!</b>";
        for (let i = 1; i <=3; i++)
        {
            document.getElementById("convTemp" + (i)).innerHTML = "";
        }
    } 
    else 
    {
        document.getElementById("inputTemperature").className = "data__input data__input--success";

        let targetUnit = scale.options[i].value;

        switch (targetUnit)
        {
            case "celsius":
                display.celsius.value = temperature,
                display.kelvin.value = temperature + 273.15,
                display.fahrenheit.value = (temperature * 9/5) + 32,
                display.rankine.value = (temperature + 273.15) * (9/5)
            break;
            
            case "kelvin":
                display.celsius.value = temperature - 273.15,
                display.kelvin.value = temperature,
                display.fahrenheit.value = (temperature * 9/5) - 459.67,
                display.rankine.value = temperature * (9/5)
            break;
            
            case "fahrenheit":
                display.celsius.value = (temperature - 32) * (5/9),
                display.kelvin.value = (temperature + 459.67) * (5/9),
                display.fahrenheit.value = temperature,
                display.rankine.value = (temperature + 459.67)
            break;
            
            case "rankine":
                display.celsius.value = (temperature - 491.67) * (5/9),
                display.kelvin.value = temperature * (5/9),
                display.fahrenheit.value = temperature - 459.67,
                display.rankine.value = temperature
            break;
        }
        
        userTemp.innerHTML = getEnteredValue().toString() + display[targetUnit].symbol;
        delete display[targetUnit];

        let keys = Object.keys(display);
        for (let i = 0; i < keys.length; i++)
        {
            let unit = keys[i];
            document.getElementById("convTemp" + (i + 1)).innerHTML = parseFloat(display[unit].value).toFixed(2) + display[unit].symbol;
        }
    }
}