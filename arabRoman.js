const convertButton = window.document.querySelector(".convert-btn");
convertButton.addEventListener("click", displayResult);

function toRoman(number){
	if(number.length == 0){ return "Wpisz liczbę, którą chcesz konwertować";}
	if(number<=0){ return "Twoja liczba jest zbyt mała."}
	if(number>3999) {return "Twoja liczba jest zbyt duża."}
	
	var arab = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
	var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
	var result = "";
	
	for (var i = 0; i<=arab.length; i++){
		while(number%arab[i] < number){
			result += roman[i];
			number -= arab[i];
		}
	}
	return result;
}
function displayResult(){
	var romanValue =toRoman(document.querySelector(".data__input").value);	
	document.querySelector(".result__value").innerHTML = romanValue;
}
	//poprawny format inputu zapewniony w HTMLU (nie mozna wpisac liter)