// Uchwyty do obsługi zdarzeń.
document.getElementById("convert").addEventListener("click", convertScript);
document.getElementById("unitFrom").addEventListener("input", modifyUnitToList);


// Przygotowanie tabel konwersji.
const convTable = {};

// Jednostki długości
convTable["Thou"] = {};
convTable["Thou"]["Thou"]       = 1.0;
convTable["Thou"]["Inch"]       = 1.0 / 1000.0;
convTable["Thou"]["Foot"]       = 1.0 / (1000.0 * 12.0);
convTable["Thou"]["Yard"]       = 1.0 / (1000.0 * 12.0 * 3.0);
convTable["Thou"]["Chain"]      = 1.0 / (1000.0 * 12.0 * 66.0);
convTable["Thou"]["Furlong"]    = 1.0 / (1000.0 * 12.0 * 660.0);
convTable["Thou"]["Mile"]       = 1.0 / (1000.0 * 12.0 * 5280.0);
convTable["Thou"]["MicroMeter"] = 25.4;
convTable["Thou"]["MiliMeter"]  = 25.4 / 1000.0;
convTable["Thou"]["CentiMeter"] = 25.4 / 10000.0;
convTable["Thou"]["Meter"]      = 25.4 / 1000000.0;
convTable["Thou"]["KiloMeter"]  = 25.4 / 1000000000;

convTable["Inch"] = {};
convTable["Inch"]["Thou"]       = 1000.0;
convTable["Inch"]["Inch"]       = 1.0;
convTable["Inch"]["Foot"]       = 1.0 / 12.0;
convTable["Inch"]["Yard"]       = 1.0 / (12.0 * 3.0);
convTable["Inch"]["Chain"]      = 1.0 / (12.0 * 66.0);
convTable["Inch"]["Furlong"]    = 1.0 / (12.0 * 660.0);
convTable["Inch"]["Mile"]       = 1.0 / (12.0 * 5280.0);
convTable["Inch"]["MicroMeter"] = 25.4 * 1000.0;
convTable["Inch"]["MiliMeter"]  = 25.4;
convTable["Inch"]["CentiMeter"] = 2.54;
convTable["Inch"]["Meter"]      = 25.4 / 1000.0;
convTable["Inch"]["KiloMeter"]  = 25.4 / 1000000.0;

convTable["Foot"] = {};
convTable["Foot"]["Thou"]       = 12.0 * 1000.0;
convTable["Foot"]["Inch"]       = 12.0;
convTable["Foot"]["Foot"]       = 1.0;
convTable["Foot"]["Yard"]       = 1.0 / 3.0;
convTable["Foot"]["Chain"]      = 1.0 / 66.0;
convTable["Foot"]["Furlong"]    = 1.0 / 660.0;
convTable["Foot"]["Mile"]       = 1.0 / 5280.0;
convTable["Foot"]["MicroMeter"] = 304.8 * 1000.0 ;
convTable["Foot"]["MiliMeter"]  = 304.8;
convTable["Foot"]["CentiMeter"] = 304.8 / 10.0;
convTable["Foot"]["Meter"]      = 304.8 / 1000.0;
convTable["Foot"]["KiloMeter"]  = 304.8 / 1000000.0;

convTable["Yard"] = {};
convTable["Yard"]["Thou"]       = 3.0 * 12.0 * 1000.0;
convTable["Yard"]["Inch"]       = 3.0 * 12.0;
convTable["Yard"]["Foot"]       = 3.0;
convTable["Yard"]["Yard"]       = 1.0;
convTable["Yard"]["Chain"]      = 3.0 / 66.0;
convTable["Yard"]["Furlong"]    = 3.0 / 660.0;
convTable["Yard"]["Mile"]       = 3.0 / 5820.0;
convTable["Yard"]["MicroMeter"] = 914.4 * 1000.0;
convTable["Yard"]["MiliMeter"]  = 914.4;
convTable["Yard"]["CentiMeter"] = 914.4 / 10.0;
convTable["Yard"]["Meter"]      = 914.4 / 1000.0;
convTable["Yard"]["KiloMeter"]  = 914.4 / 1000000.0;

convTable["Chain"] = {};
convTable["Chain"]["Thou"]       = 66.0 * 12.0 * 1000.0;
convTable["Chain"]["Inch"]       = 66.0 * 12.0;
convTable["Chain"]["Foot"]       = 66.0;
convTable["Chain"]["Yard"]       = 22.0;
convTable["Chain"]["Chain"]      = 1.0;
convTable["Chain"]["Furlong"]    = 1.0 / 10.0;
convTable["Chain"]["Mile"]       = 1.0 / 80.0;
convTable["Chain"]["MicroMeter"] = 20.1168 * 1000000.0;
convTable["Chain"]["MiliMeter"]  = 20.1168 * 1000.0;
convTable["Chain"]["CentiMeter"] = 20.1168 * 100.0;
convTable["Chain"]["Meter"]      = 20.1168;
convTable["Chain"]["KiloMeter"]  = 20.1168 / 1000.0;

convTable["Furlong"] = {};
convTable["Furlong"]["Thou"]       = 660.0 * 12.0 * 1000.0;
convTable["Furlong"]["Inch"]       = 660.0 * 12.0;
convTable["Furlong"]["Foot"]       = 660.0;
convTable["Furlong"]["Yard"]       = 220.0;
convTable["Furlong"]["Chain"]      = 10.0;
convTable["Furlong"]["Furlong"]    = 1.0;
convTable["Furlong"]["Mile"]       = 1.0 / 8.0;
convTable["Furlong"]["MicroMeter"] = 201.168 * 1000000.0;
convTable["Furlong"]["MiliMeter"]  = 201.168 * 1000.0;
convTable["Furlong"]["CentiMeter"] = 201.168 * 10.0;
convTable["Furlong"]["Meter"]      = 201.168;
convTable["Furlong"]["KiloMeter"]  = 201.168 / 1000.0;

convTable["Mile"] = {};
convTable["Mile"]["Inch"]       = 80.0 * 22.0 * 3.0 * 12.0 * 1000.0;
convTable["Mile"]["Thou"]       = 80.0 * 22.0 * 3.0 * 12.0;
convTable["Mile"]["Foot"]       = 80.0 * 22.0 * 3.0;
convTable["Mile"]["Yard"]       = 80.0 * 22.0;
convTable["Mile"]["Chain"]      = 80.0;
convTable["Mile"]["Furlong"]    = 8.0;
convTable["Mile"]["Mile"]       = 1.0;
convTable["Mile"]["MicroMeter"] = 1609.344 * 1000000.0;
convTable["Mile"]["MiliMeter"]  = 1609.344 * 1000.0;
convTable["Mile"]["CentiMeter"] = 1609.344 * 10.0;
convTable["Mile"]["Meter"]      = 1609.344;
convTable["Mile"]["KiloMeter"]  = 1609.344 / 1000.0;

convTable["MicroMeter"] = {};
convTable["MicroMeter"]["Thou"]       = 1.0;
convTable["MicroMeter"]["Inch"]       = 1.0;
convTable["MicroMeter"]["Foot"]       = 1.0;
convTable["MicroMeter"]["Yard"]       = 1.0;
convTable["MicroMeter"]["Chain"]      = 1.0;
convTable["MicroMeter"]["Furlong"]    = 1.0;
convTable["MicroMeter"]["Mile"]       = 1.0;
convTable["MicroMeter"]["MicroMeter"] = 1.0;
convTable["MicroMeter"]["MiliMeter"]  = 1.0 / 1000.0;
convTable["MicroMeter"]["CentiMeter"] = 1.0 / 10000.0;
convTable["MicroMeter"]["Meter"]      = 1.0 / 1000000.0;
convTable["MicroMeter"]["KiloMeter"]  = 1.0 / 1000000000;

convTable["MiliMeter"] = {};
convTable["MiliMeter"]["Thou"]       = 1.0 / (25.4 * 1000.0);
convTable["MiliMeter"]["Inch"]       = 1.0 / 25.4;
convTable["MiliMeter"]["Foot"]       = 1.0 / 304.8;
convTable["MiliMeter"]["Yard"]       = 1.0 / 914.4;
convTable["MiliMeter"]["Chain"]      = 1000.0 / 20.1168;
convTable["MiliMeter"]["Furlong"]    = 1000.0 / 201.168;
convTable["MiliMeter"]["Mile"]       = 1000.0 / 1609.344;
convTable["MiliMeter"]["MicroMeter"] = 1000.0;
convTable["MiliMeter"]["MiliMeter"]  = 1.0;
convTable["MiliMeter"]["CentiMeter"] = 1.0 / 10.0;
convTable["MiliMeter"]["Meter"]      = 1.0 / 1000.0;
convTable["MiliMeter"]["KiloMeter"]  = 1.0 / 1000000.0;

convTable["CentiMeter"] = {};
convTable["CentiMeter"]["Thou"]       = 10.0 / (25.4 * 1000.0);
convTable["CentiMeter"]["Inch"]       = 10.0 / 25.4;
convTable["CentiMeter"]["Foot"]       = 10.0 / 304.8;
convTable["CentiMeter"]["Yard"]       = 10.0 / 914.4;
convTable["CentiMeter"]["Chain"]      = 10000.0 / 20.1168;
convTable["CentiMeter"]["Furlong"]    = 10000.0 / 201.168;
convTable["CentiMeter"]["Mile"]       = 10000.0 / 1609.344;
convTable["CentiMeter"]["MicroMeter"] = 10000.0;
convTable["CentiMeter"]["MiliMeter"]  = 10.0;
convTable["CentiMeter"]["CentiMeter"] = 1.0;
convTable["CentiMeter"]["Meter"]      = 1.0 / 100.0;
convTable["CentiMeter"]["KiloMeter"]  = 1.0 / 100000.0;

convTable["Meter"] = {};
convTable["Meter"]["Thou"]       = 1000.0 / (25.4 * 1000.0);
convTable["Meter"]["Inch"]       = 1000.0 / 25.4;
convTable["Meter"]["Foot"]       = 1000.0 / 304.8;
convTable["Meter"]["Yard"]       = 1000.0 / 914.4;
convTable["Meter"]["Chain"]      = 1.0 / 20.1168;
convTable["Meter"]["Furlong"]    = 1.0 / 201.168;
convTable["Meter"]["Mile"]       = 1.0 / 1609.344;
convTable["Meter"]["MicroMeter"] = 1000000.0;
convTable["Meter"]["MiliMeter"]  = 1000.0;
convTable["Meter"]["CentiMeter"] = 100.0;
convTable["Meter"]["Meter"]      = 1.0;
convTable["Meter"]["KiloMeter"]  = 1.0 / 1000.0;

convTable["KiloMeter"] = {};
convTable["KiloMeter"]["Thou"]       = 1000000.0 / (25.4 * 1000.0);
convTable["KiloMeter"]["Inch"]       = 1000000.0 / 25.4;
convTable["KiloMeter"]["Foot"]       = 1000000.0 / 304.8;
convTable["KiloMeter"]["Yard"]       = 1000000.0 / 914.4;
convTable["KiloMeter"]["Chain"]      = 1000.0 / 20.1168;
convTable["KiloMeter"]["Furlong"]    = 1000.0 / 201.168;
convTable["KiloMeter"]["Mile"]       = 1000.0 / 1609.344;
convTable["KiloMeter"]["MicroMeter"] = 1.0 / 1000000000.0;
convTable["KiloMeter"]["MiliMeter"]  = 1.0 / 1000000.0;
convTable["KiloMeter"]["CentiMeter"] = 1.0 / 100000.0;
convTable["KiloMeter"]["Meter"]      = 1.0 / 1000.0;
convTable["KiloMeter"]["KiloMeter"]  = 1.0;

// Jednostki powierzchni
convTable["Perch"] = {};
convTable["Perch"]["Perch"]       = 1.0;
convTable["Perch"]["Rood"]        = 1.0 / 40.0;
convTable["Perch"]["Acre"]        = 1.0 / 160.0;
convTable["Perch"]["SqMeter"]     = 25.29285264;
convTable["Perch"]["SqKilometer"] = 25.29285264 / 1000000.0;
convTable["Perch"]["Hectare"]     = 25.29285264 / 10000.0;

convTable["Rood"] = {};
convTable["Rood"]["Perch"]       = 40.0;
convTable["Rood"]["Rood"]        = 1.0;
convTable["Rood"]["Acre"]        = 40.0 / 160.0;
convTable["Rood"]["SqMeter"]     = 1011.7141056;
convTable["Rood"]["SqKilometer"] = 1011.7141056 / 1000000.0;
convTable["Rood"]["Hectare"]     = 1011.7141056 / 10000.0;

convTable["Acre"] = {};
convTable["Acre"]["Perch"]       = 160.0;
convTable["Acre"]["Rood"]        = 160.0 / 40.0;
convTable["Acre"]["Acre"]        = 1.0;
convTable["Acre"]["SqMeter"]     = 4046.8564224;
convTable["Acre"]["SqKilometer"] = 4046.8564224 / 1000000.0;
convTable["Acre"]["Hectare"]     = 4046.8564224 / 10000.0;

convTable["SqMeter"] = {};
convTable["SqMeter"]["Perch"]       = 1.0 / 25.29285264;
convTable["SqMeter"]["Rood"]        = 1.0 / 1011.7141056;
convTable["SqMeter"]["Acre"]        = 1.0 / 4046.8564224;
convTable["SqMeter"]["SqMeter"]     = 1.0;
convTable["SqMeter"]["SqKilometer"] = 0.000001;
convTable["SqMeter"]["Hectare"]     = 0.0001;

convTable["SqKilometer"] = {};
convTable["SqKilometer"]["Perch"]       = 1000000.0 / 25.29285264;
convTable["SqKilometer"]["Rood"]        = 1000000.0 / 1011.7141056;
convTable["SqKilometer"]["Acre"]        = 1000000.0 / 4046.8564224;
convTable["SqKilometer"]["SqMeter"]     = 1000000.0;
convTable["SqKilometer"]["SqKilometer"] = 1.0;
convTable["SqKilometer"]["Hectare"]     = 100.0;

convTable["Hectare"] = {};
convTable["Hectare"]["Perch"]       = 10000.0 / 25.29285264;
convTable["Hectare"]["Rood"]        = 10000.0 / 1011.7141056;
convTable["Hectare"]["Acre"]        = 10000.0 / 4046.8564224;
convTable["Hectare"]["SqMeter"]     = 10000.0;
convTable["Hectare"]["SqKilometer"] = 1.0 / 100.0;
convTable["Hectare"]["Hectare"]     = 1.0;

// Jednostki objętości
convTable["FluidOunce"] = {};
convTable["FluidOunce"]["FluidOunce"]    = 1.0;
convTable["FluidOunce"]["Gill"]          = 1.0 / 5.0;
convTable["FluidOunce"]["Pint"]          = 1.0 / (5.0 * 4.0);
convTable["FluidOunce"]["Quart"]         = 1.0 / (5.0 * 4.0 * 2.0);
convTable["FluidOunce"]["Gallon"]        = 1.0 / (5.0 * 4.0 * 2.0 * 4.0);
convTable["FluidOunce"]["CubeMilimeter"] = (28.4130625) * 1000.0;
convTable["FluidOunce"]["Litre"]         = (28.4130625) / 1000.0;
convTable["FluidOunce"]["CubeMeter"]     = (28.4130625) / 1000000.0;

convTable["Gill"] = {};
convTable["Gill"]["FluidOunce"]    = 5.0;
convTable["Gill"]["Gill"]          = 1.0;
convTable["Gill"]["Pint"]          = 1.0 / 4.0;
convTable["Gill"]["Quart"]         = 1.0 / (4.0 * 2.0);
convTable["Gill"]["Gallon"]        = 1.0 / (4.0 * 2.0 * 4.0);
convTable["Gill"]["Peck"]          = 1.0 / (4.0 * 2.0 * 4.0 * 2.0);
convTable["Gill"]["Bushel"]        = 1.0 / (4.0 * 2.0 * 4.0 * 2.0 * 4.0);
convTable["Gill"]["CubeMilimeter"] = (5.0 * 28.4130625) * 1000.0;
convTable["Gill"]["Litre"]         = (5.0 * 28.4130625) / 1000.0;
convTable["Gill"]["CubeMeter"]     = (5.0 * 28.4130625) / 1000000.0;

convTable["Pint"] = {};
convTable["Pint"]["FluidOunce"]    = 5.0 * 4.0;
convTable["Pint"]["Gill"]          = 4.0;
convTable["Pint"]["Pint"]          = 1.0;
convTable["Pint"]["Quart"]         = 1.0 / 2.0;
convTable["Pint"]["Gallon"]        = 1.0 / (2.0 * 4.0);
convTable["Pint"]["Peck"]          = 1.0 / (2.0 * 4.0 * 2.0);
convTable["Pint"]["Bushel"]        = 1.0 / (2.0 * 4.0 * 2.0 * 4.0);
convTable["Pint"]["CubeMilimeter"] = (5.0 * 4.0 * 28.4130625) * 1000.0;
convTable["Pint"]["Litre"]         = (5.0 * 4.0 * 28.4130625) / 1000.0;
convTable["Pint"]["CubeMeter"]     = (5.0 * 4.0 * 28.4130625) / 1000000.0;

convTable["Quart"] = {};
convTable["Quart"]["FluidOunce"]    = 5.0 * 4.0 * 2.0;
convTable["Quart"]["Gill"]          = 4.0 * 2.0;
convTable["Quart"]["Pint"]          = 2.0;
convTable["Quart"]["Quart"]         = 1.0;
convTable["Quart"]["Gallon"]        = 1.0 / 4.0;
convTable["Quart"]["Peck"]          = 1.0 / (4.0 * 2.0);
convTable["Quart"]["Bushel"]        = 1.0 / (4.0 * 2.0 * 4.0);
convTable["Quart"]["CubeMilimeter"] = (5.0 * 4.0 * 2.0 * 28.4130625) * 1000.0;
convTable["Quart"]["Litre"]         = (5.0 * 4.0 * 2.0 * 28.4130625) / 1000.0;
convTable["Quart"]["CubeMeter"]     = (5.0 * 4.0 * 2.0 * 28.4130625) / 1000000.0;

convTable["Gallon"] = {};
convTable["Gallon"]["FluidOunce"]    = 5.0 * 4.0 * 2.0 * 4.0;
convTable["Gallon"]["Gill"]          = 4.0 * 2.0 * 4.0;
convTable["Gallon"]["Pint"]          = 2.0 * 4.0;
convTable["Gallon"]["Quart"]         = 4.0;
convTable["Gallon"]["Gallon"]        = 1.0;
convTable["Gallon"]["Peck"]          = 1.0 / 2.0;
convTable["Gallon"]["Bushel"]        = 1.0 / (2.0 * 4.0);
convTable["Gallon"]["CubeMilimeter"] = (5.0 * 4.0 * 2.0 * 4.0 * 28.4130625) * 1000.0;
convTable["Gallon"]["Litre"]         = (5.0 * 4.0 * 2.0 * 4.0 * 28.4130625) / 1000.0;
convTable["Gallon"]["CubeMeter"]     = (5.0 * 4.0 * 2.0 * 4.0 * 28.4130625) / 1000000.0;

convTable["Peck"] = {};
convTable["Peck"]["Gill"]          = 4.0 * 2.0 * 4.0 * 2.0;
convTable["Peck"]["Pint"]          = 4.0 * 2.0 * 4.0;
convTable["Peck"]["Quart"]         = 4.0 * 2.0;
convTable["Peck"]["Gallon"]        = 4.0;
convTable["Peck"]["Peck"]          = 1.0;
convTable["Peck"]["Bushel"]        = 1.0 / 4.0;
convTable["Peck"]["CubeMilimeter"] = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 28.4130625) * 1000.0;
convTable["Peck"]["Litre"]         = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 28.4130625) / 1000.0;
convTable["Peck"]["CubeMeter"]     = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 28.4130625) / 1000000.0;

convTable["Bushel"] = {};
convTable["Bushel"]["Gill"]          = 4.0 * 2.0 * 4.0 * 2.0 * 4.0;
convTable["Bushel"]["Pint"]          = 4.0 * 2.0 * 4.0 * 2.0;
convTable["Bushel"]["Quart"]         = 4.0 * 2.0 * 4.0;
convTable["Bushel"]["Gallon"]        = 4.0 * 2.0;
convTable["Bushel"]["Peck"]          = 4.0;
convTable["Bushel"]["Bushel"]        = 1.0;
convTable["Bushel"]["CubeMilimeter"] = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0 * 28.4130625) * 1000.0;
convTable["Bushel"]["Litre"]         = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0 * 28.4130625) / 1000.0;
convTable["Bushel"]["CubeMeter"]     = (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0 * 28.4130625) / 1000000.0;

convTable["CubeMilimeter"] = {};
convTable["CubeMilimeter"]["FluidOunce"]    = 28.4130625;
convTable["CubeMilimeter"]["Gill"]          = 28.4130625 / (5.0);
convTable["CubeMilimeter"]["Pint"]          = 28.4130625 / (5.0 * 4.0);
convTable["CubeMilimeter"]["Quart"]         = 28.4130625 / (5.0 * 4.0 * 2.0);
convTable["CubeMilimeter"]["Gallon"]        = 28.4130625 / (5.0 * 4.0 * 2.0 * 4.0);
convTable["CubeMilimeter"]["Peck"]          = 28.4130625 / (5.0 * 4.0 * 2.0 * 4.0 * 2.0);
convTable["CubeMilimeter"]["Bushel"]        = 28.4130625 / (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0);
convTable["CubeMilimeter"]["CubeMilimeter"] = 1.0;
convTable["CubeMilimeter"]["Litre"]         = 1.0 / 1000000.0;
convTable["CubeMilimeter"]["CubeMeter"]     = 1.0 / 1000000000.0;

convTable["Litre"] = {};
convTable["Litre"]["FluidOunce"]    = (28.4130625 * 1000000.0);
convTable["Litre"]["Gill"]          = (28.4130625 * 1000000.0) / (5.0);
convTable["Litre"]["Pint"]          = (28.4130625 * 1000000.0) / (5.0 * 4.0);
convTable["Litre"]["Quart"]         = (28.4130625 * 1000000.0) / (5.0 * 4.0 * 2.0);
convTable["Litre"]["Gallon"]        = (28.4130625 * 1000000.0) / (5.0 * 4.0 * 2.0 * 4.0);
convTable["Litre"]["Peck"]          = (28.4130625 * 1000000.0) / (5.0 * 4.0 * 2.0 * 4.0 * 2.0);
convTable["Litre"]["Bushel"]        = (28.4130625 * 1000000.0) / (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0);
convTable["Litre"]["CubeMilimeter"] = 1000000.0;
convTable["Litre"]["Litre"]         = 1.0;
convTable["Litre"]["CubeMeter"]     = 1.0 / 1000.0;

convTable["CubeMeter"] = {};
convTable["CubeMeter"]["FluidOunce"]    = (28.4130625 * 1000000000.0);
convTable["CubeMeter"]["Gill"]          = (28.4130625 * 1000000000.0) / (5.0);
convTable["CubeMeter"]["Pint"]          = (28.4130625 * 1000000000.0) / (5.0 * 4.0);
convTable["CubeMeter"]["Quart"]         = (28.4130625 * 1000000000.0) / (5.0 * 4.0 * 2.0);
convTable["CubeMeter"]["Gallon"]        = (28.4130625 * 1000000000.0) / (5.0 * 4.0 * 2.0 * 4.0);
convTable["CubeMeter"]["Peck"]          = (28.4130625 * 1000000000.0) / (5.0 * 4.0 * 2.0 * 4.0 * 2.0);
convTable["CubeMeter"]["Bushel"]        = (28.4130625 * 1000000000.0) / (5.0 * 4.0 * 2.0 * 4.0 * 2.0 * 4.0);
convTable["CubeMeter"]["CubeMilimeter"] = 1000000000.0;
convTable["CubeMeter"]["Litre"]         = 1000.0;
convTable["CubeMeter"]["CubeMeter"]     = 1.0;


// Pobranie obiektów z danymi z ,,formularza''.
const inputText  = document.getElementById("inputValue");
const unitFrom   = document.getElementById("unitFrom");
const unitTo     = document.getElementById("unitTo");
const resultText = document.getElementById("resultValue");
    

// Wyznaczenie wartości konwertowanej danej i wyświetlenie na stronie.
function convertScript() {
    // Wczytaj dane z ,,formularza''.
    const inputNum     = parseFloat(inputText.value);
    const unitFromIdx  = unitFrom.selectedOptions[0].value;
    const unitFromName = unitFrom.selectedOptions[0].innerText;
    const unitToIdx    = unitTo.selectedOptions[0].value;
    const unitToName   = unitTo.selectedOptions[0].innerText;

    // Przygotuj zmienne lokalne.
    let conversion   = 1.0;
    let convResult   = 0.0;

    // Wypis na konsolę:
    console.log("  ====  Próba przeliczenia jednostek  ====")
    console.log("inputValue  = " + inputText.value);
    console.log("unitFromIdx = " + unitFromIdx);
    console.log("unitToIdx   = " + unitToIdx);

    // Przetwórz pobrane dane.
    // Jeśli wprowadzona liczba nie jest w rzeczywistości liczbą, nakaż użytkownikowi
    // poprawić wprowadzone dane.
    if (isNaN(inputNum)) {
        alert("Podana wielkość nie jest wartością liczbową! Podaj liczbę!");
        inputText.className = "data__input data__input--error";

        // Wypis na konsolę:
        // console.log("BŁĄD: " + inputText.value + " nie jest liczbą!");
    }
    else {
        // Sukces! Użytkownik wprowadził poprawne dane.
        // Pobierzmy współczynnik przeliczenia i wyznaczmy końcową wartość.
        // Jeśli współczynnik nie istnieje - znaczy, że użytkownik wybrał niezgodne jednostki.
        inputText.className = "data__input data__input--success";

        try {
            conversion = convTable[unitFromIdx][unitToIdx];

            // Wypis na konsolę:
            // console.log("conversion  = " + conversion);

            if (typeof conversion === "undefined") {
                alert("Próbujesz przeliczyć jednostki różnego typu! Nie mieszaj np. długości z powierzchnią, ani masy z objętością!");
                unitFrom.className = "data__input data__input--error";
                unitTo.className = "data__input data__input--error";
            
                // Wypis na konsolę:
                // console.log("BŁĄD: Próba przeliczenia jednostek niezgodnego typu!");
            }
            else {
                unitFrom.className = "data__input data__input--success";
                unitTo.className = "data__input data__input--success";
    
                convResult = inputNum * conversion;
                resultText.innerHTML = inputNum + " " + unitFromName + " = " + convResult + " " + unitToName;

                // Wypis na konsolę:
                // console.log("convResult  = " + convResult);
            }
        }
        catch {
            alert("Nastąpiła nieprzewidziana sytuacja...");
            unitFrom.className = "data__input data__input--error";
            unitTo.className = "data__input data__input--error";

            // Wypis na konsolę:
            // console.log("WYJĄTEK: Nastąpiła nieprzewidziana sytuacja...");
        }
    }
}


// Zmiana listy opcji docelowych jednostek konwersji.
function modifyUnitToList() {
    let idx = 0;
    var option = {};

    // Wyczyść wszystkie opcje.
    for (idx = (unitTo.length - 1); unitTo.length > 0; idx--) {
        unitTo.remove(idx);
    }

    // Wprowadź poprawne opcje na listę.
    switch (unitFrom.value) {
        case "MicroMeter":
        case "MiliMeter":
        case "CentiMeter":
        case "Meter":
        case "KiloMeter":
        case "Thou":
        case "Inch":
        case "Foot":
        case "Yard":
        case "Chain":
        case "Furlong":
        case "Mile":
            option = document.createElement("option");
                option.value = "MicroMeter";
                option.innerHTML = "um";
                option.selected = true;
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "MiliMeter";
                option.innerHTML = "mm";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "CentiMeter";
                option.innerHTML = "cm";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Meter";
                option.innerHTML = "m";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "KiloMeter";
                option.innerHTML = "km";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Thou";
                option.innerHTML = "Thou";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Inch";
                option.innerHTML = "Cal";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Foot";
                option.innerHTML = "Stopa";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Yard";
                option.innerHTML = "Jard";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Chain";
                option.innerHTML = "Łańcuch";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Furlong";
                option.innerHTML = "Furlong";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Mile";
                option.innerHTML = "Mila";
                unitTo.add(option);
            break;
        case "SqMeter":
        case "SqKilometer":
        case "Hectare":
        case "Perch":
        case "Rood":
        case "Acre":
            option = document.createElement("option");
                option.value = "SqMeter";
                option.innerHTML = "m^2";
                option.selected = true;
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "SqKilometer";
                option.innerHTML = "km^2";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Hectare";
                option.innerHTML = "ha";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Perch";
                option.innerHTML = "Perch";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Rood";
                option.innerHTML = "Rood";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Acre";
                option.innerHTML = "Akr";
                unitTo.add(option);
            break;
        case "CubeMilimeter":
        case "Litre":
        case "CubeMeter":
        case "Gill":
        case "Pint":
        case "Quart":
        case "Gallon":
        case "Peck":
            option = document.createElement("option");
                option.value = "CubeMilimeter";
                option.innerHTML = "mm^3";
                option.selected = true;
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Litre";
                option.innerHTML = "L";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "CubeMeter";
                option.innerHTML = "m^3";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "FluidOunce";
                option.innerHTML = "fl oz";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gill";
                option.innerHTML = "Gill";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Pint";
                option.innerHTML = "Pinta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Quart";
                option.innerHTML = "Kwarta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gallon";
                option.innerHTML = "Galon";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Peck";
                option.innerHTML = "Peck";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Bushel";
                option.innerHTML = "Buszel";
                unitTo.add(option);
            break;
        case "FluidOunce":
                option = document.createElement("option");
                option.value = "CubeMilimeter";
                option.innerHTML = "mm^3";
                option.selected = true;
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Litre";
                option.innerHTML = "L";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "CubeMeter";
                option.innerHTML = "m^3";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "FluidOunce";
                option.innerHTML = "fl oz";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gill";
                option.innerHTML = "Gill";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Pint";
                option.innerHTML = "Pinta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Quart";
                option.innerHTML = "Kwarta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gallon";
                option.innerHTML = "Galon";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Peck";
                option.innerHTML = "Peck";
                unitTo.add(option);
            break;
        case "Bushel":
                option = document.createElement("option");
                option.value = "CubeMilimeter";
                option.innerHTML = "mm^3";
                option.selected = true;
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Litre";
                option.innerHTML = "L";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "CubeMeter";
                option.innerHTML = "m^3";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gill";
                option.innerHTML = "Gill";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Pint";
                option.innerHTML = "Pinta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Quart";
                option.innerHTML = "Kwarta";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Gallon";
                option.innerHTML = "Galon";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Peck";
                option.innerHTML = "Peck";
                unitTo.add(option);
            option = document.createElement("option");
                option.value = "Bushel";
                option.innerHTML = "Buszel";
                unitTo.add(option);
            break;
        default:
            option = document.createElement("option");
                option.value = "--";
                option.innerHTML = "--";
                unitTo.add(option);
    }
}
