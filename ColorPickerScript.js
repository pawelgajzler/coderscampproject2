const inp_err = "data__input data__input--error";
const inp_suc = "data__input data__input--success";
const inp_inc = "ICORRECT INPUT!";
const newline = "<br></br>";
const chooseopt = "CHOOSE OPTION !";
const anspart1 = "<br>YOU NEED TO FILL ";
const anspart2 = " FIELD VIA ";
const ansarr = ["NUMBER ","CHARACTERS "];
const rangearr = ["[0-255]","[0-360]","[0.00-1.00]","[0-9 , a-f , A-F]","[a-z , A-Z]"];
const titlearr = ["(NON NUMBER)","W/O '#'","DEG"];

var btn = document.querySelector(".convert-btn");
var options = document.querySelector("#color-cnv-opt");
var data = document.querySelector("#content_data");
var options_data = document.querySelector("#options_data");
var options_data_canv = options_data.getElementsByTagName("canvas");
var options_data_inp = options_data.getElementsByTagName("input");
var result = document.querySelector(".result");
var result_lb = document.querySelector(".result__label");
var result_vl = document.querySelector(".result__value");
var clrblock = document.querySelector("#clr1");
var ele;

var temp_arr=[];

options.value = "-1";

btn.addEventListener("click", () => {
	removeAllChildren(result);

	switch(options.value){
		case '0':
			var r = document.querySelector("#RED");
			var g = document.querySelector("#GREEN");
			var b = document.querySelector("#BLUE"); 
			var re = r.value;
			var ge = g.value;
			var be = b.value;
			var err;

			if(checkLenAndVal(re)) {
				r.className = inp_err;
				result.innerHTML += createAnswear(r.id, rangearr[0], ansarr[0]);
			}
			else r.className = inp_suc;

			if(checkLenAndVal(ge)) {
				g.className = inp_err;
				result.innerHTML += createAnswear(g.id, rangearr[0], ansarr[0]);
			}
			else g.className = inp_suc;

			if(checkLenAndVal(be)) {
				b.className = inp_err;
				result.innerHTML += createAnswear(b.id, rangearr[0], ansarr[0]);
			}
			else b.className = inp_suc;

			if(checkLenAndVal(re, ge, be)) {
				err = result.innerHTML;
				result.innerHTML = newline;
				result.innerHTML += inp_inc + err;
			}
			else {
				result.innerHTML = newline;
				fromRGB(re, ge, be);
			}
		break;

		case '1':
			var name = document.querySelector("#COLOR_NAME");
			name.value = name.value.toUpperCase();
			if(checkLenAndVal(name.value)) {
				name.className = inp_err;
				result.innerHTML = inp_inc;
				result.innerHTML += createAnswear(name.id, rangearr[4], ansarr[1], titlearr[0]);
			}
			else {
				if(!isNaN(name.value)) {
					name.className = inp_err;
					result.innerHTML = inp_inc;
					result.innerHTML += createAnswear(name.id, rangearr[4], ansarr[1], titlearr[0]);
				}
				else if(checkNameExist(name.value)) {
					name.className = inp_err;
					result.innerHTML = "CAN'T FIND "+ name.value + " COLOUR IN DATABASE";
				}
				else {
				name.className = inp_suc;
				result.innerHTML = newline;
				fromTEXT(name.value);
				}
			}
		break;

		case '2':
			var hex = document.querySelector("#HEXe");
			hex.value = hex.value.toUpperCase();
			if(checkLenAndVal(hex.value)) {
				hex.className = inp_suc;
				result.innerHTML = newline;
				fromHEX(hex.value);
			}
			else {
				result.innerHTML = newline;
				hex.className = inp_err;
				result.innerHTML = inp_inc;
				result.innerHTML += createAnswear(hex.id, rangearr[3], ansarr[1], titlearr[1]);
			}
		break;

		case '3':
			var hue = document.querySelector("#HUE1");
			var sat = document.querySelector("#SATURATION1");
			var light = document.querySelector("#LIGHTNESS");
			var huev = hue.value;
			var satv = sat.value;
			var lightv = light.value;

			if(checkLenAndVal(huev)) {
				hue.className = inp_err;
				result.innerHTML += createAnswear(hue.id, rangearr[1], ansarr[0], titlearr[2]);
			}
			else hue.className = inp_suc;

			if(checkLenAndVal(satv) || checkVal(satv, 0 , 1)) {
				sat.className = inp_err;
				result.innerHTML += createAnswear(sat.id, rangearr[2], ansarr[0]);
			}
			else sat.className = inp_suc;

			if(checkLenAndVal(lightv) || checkVal(lightv, 0 , 1)) {
				light.className = inp_err;
				result.innerHTML += createAnswear(light.id, rangearr[2], ansarr[0]);
			}
			else light.className = inp_suc;

			if(checkLenAndVal(huev, satv, lightv) || checkVal(satv, 0 , 1) || checkVal(lightv, 0 , 1)) {
				err = result.innerHTML;
				result.innerHTML = newline;
				result.innerHTML += inp_inc + err;
			}
			else {
				result.innerHTML = newline;
				fromHSL(huev,satv,lightv);
			}
		break;

		case '4':
			var hue = document.querySelector("#HUE2");
			var sat = document.querySelector("#SATURATION2");
			var val = document.querySelector("#VALUE");
			var huev = hue.value;
			var satv = sat.value;
			var valv = val.value;

			if(checkLenAndVal(huev)) {
				hue.className = inp_err;
				result.innerHTML += createAnswear(hue.id, rangearr[1], ansarr[0], titlearr[2]);
			}
			else hue.className = inp_suc;

			if(checkLenAndVal(satv) || checkVal(satv, 0 , 1)) {
			sat.className = inp_err;
			result.innerHTML += createAnswear(sat.id, rangearr[2], ansarr[0]);
			}
			else sat.className = inp_suc;

			if(checkLenAndVal(valv) || checkVal(valv, 0 , 1)) {
			val.className = inp_err;
			result.innerHTML += createAnswear(val.id, rangearr[2], ansarr[0]);
			}
			else val.className = inp_suc;

			if(checkLenAndVal(huev, satv, valv) || checkVal(satv, 0 , 1) || checkVal(valv, 0 , 1)) {
			err = result.innerHTML;
			result.innerHTML = newline;
			result.innerHTML += inp_inc + err;
			}
			else {
				result.innerHTML = newline;
				fromHSV(huev,satv,valv);
			}
		break;

		case '5':
			var c = document.querySelector("#CYAN");
			var m = document.querySelector("#MAGENTA");
			var y = document.querySelector("#YELLOW");
			var k = document.querySelector("#KEY_COLOUR");
			var cv = c.value;
			var mv = m.value;
			var yv = y.value;
			var kv = k.value;

			if(checkLenAndVal(cv)) {
				c.className = inp_err;
				result.innerHTML += createAnswear(c.id, rangearr[2], ansarr[0]);
			}
			else c.className = inp_suc;

			if(checkLenAndVal(mv)) {
				m.className = inp_err;
				result.innerHTML += createAnswear(m.id, rangearr[2], ansarr[0]);
			}
			else m.className = inp_suc;
		
			if(checkLenAndVal(yv)) {
				y.className = inp_err;
				result.innerHTML += createAnswear(y.id, rangearr[2], ansarr[0]);
			}
			else y.className = inp_suc;

			if(checkLenAndVal(kv)) {
				k.className = inp_err;
				result.innerHTML += createAnswear(k.id, rangearr[2], ansarr[0]);
			}
			else k.className = inp_suc;

			if(checkLenAndVal(cv, mv, yv, kv)){
				err = result.innerHTML;
				result.innerHTML = newline;
				result.innerHTML += inp_inc + err;
			}
			else{
				result.innerHTML = newline;
				fromCMYK(cv,mv,yv,kv);
			}
		break;

		case '6':
			var hex = document.querySelector("#HEXe");
			hex.value = hex.value.toUpperCase();
			if(checkLenAndVal(hex.value)){
				hex.className = inp_suc;
				result.innerHTML = newline;
				document.querySelector("#clr1").value = "#" + hex.value;
  				document.querySelector(".color-palette").style.backgroundColor = "#" + hex.value;
				fromHEX(hex.value);
			}
			else{
				result.innerHTML = newline;
				hex.className = inp_err;
				result.innerHTML = inp_inc;
				result.innerHTML += createAnswear(hex.id, rangearr[3], ansarr[1], titlearr[1]);
			}
		break;

		default:
			result.innerHTML = chooseopt;
		break;
	}
});

options.addEventListener("change", () => {

	document.querySelector(".data__label").innerHTML = "Select converter:";
	removeAllChildren(data);

	if(options_data_canv.length == 1) {
		options_data.removeChild(options_data_inp[0]);
		options_data.removeChild(options_data_canv[0]);
	}

	switch(options.value) {
		case '0':
			result.innerHTML="RGB";
			createLabelnInput(data, "RED", rangearr[0]);
			document.querySelector("#RED").type = "number";
			createLabelnInput(data, "GREEN", rangearr[0]);
			document.querySelector("#GREEN").type = "number";
			createLabelnInput(data, "BLUE", rangearr[0]);
			document.querySelector("#BLUE").type = "number";
		break;

		case '1':
			result.innerHTML="TEXT";
			createLabelnInput(data, "COLOR_NAME", ("NAME " + rangearr[4]));
		break;

		case '2':
			result.innerHTML="HEX";
			createLabelnInput(data, "HEXe", ("6 CHARACTERS " + rangearr[3] + " " + titlearr[1]));
		break;

		case '3':
			result.innerHTML="HSL";
			createLabelnInput(data, "HUE1", (rangearr[1] + " " + titlearr[2]));
			document.querySelector("#HUE1").type = "number";
			createLabelnInput(data, "SATURATION1", rangearr[2]);
			createLabelnInput(data, "LIGHTNESS", rangearr[2]);
		break;

		case '4':
			result.innerHTML="HSV";
			createLabelnInput(data, "HUE2", (rangearr[1] + " " + titlearr[2]));
			document.querySelector("#HUE2").type = "number";
			createLabelnInput(data, "SATURATION2", rangearr[2]);
			createLabelnInput(data, "VALUE", rangearr[2]);
		break;

		case '5':
			result.innerHTML="CMYK";
			createLabelnInput(data, "CYAN", rangearr[2]);
			createLabelnInput(data, "MAGENTA", rangearr[2]);
			createLabelnInput(data, "YELLOW", rangearr[2]);
			createLabelnInput(data, "KEY_COLOUR", rangearr[2]);
		break;

		case '6':
			result.innerHTML="COLOR_PICKER";
			createCanvas(options_data);
			createLabelnInput(data, "HEXe", ("6 CHARACTERS " + rangearr[3] + " " + titlearr[1]));
			document.querySelector(".data__label").innerHTML = "Select colour<br></br><br></br>Select 						converter:";
			document.querySelector("#clr1").addEventListener("input", () =>{
				var hex_val;
  				hex_val = document.querySelector("#clr1").value;
  				document.querySelector(".color-palette").style.backgroundColor = hex_val;
				document.querySelector("#HEXe").className = inp_suc;
  				hex_val = hex_val.toString(16);
  				hex_val = hex_val.replace("#", "");
  				document.querySelector("#HEXe").value = hex_val.toUpperCase();
  				fromHEX(hex_val);
			});
		break;

		default:
		result.innerHTML = chooseopt;
		break;
	}
}); 

// BUILDERS

function createAnswear(name, range, ans, title){
	var _result = "";

	switch(arguments.length){
		case 3:
			_result = anspart1 + arguments[0] + anspart2 + arguments[2] + arguments[1] + " </br>";
			return _result;
		break;

		case 4:
			_result = anspart1 + arguments[0] + anspart2 + arguments[2] + arguments[1] + " " + arguments[3] + " </br>"; 
			return _result;
		break;
	}
}

function checkLen(st, maxlen){
	if(st.length == 0 || st.length > maxlen) return true;
	else return false;
}

function checkVal(st, minval, maxval){
	if(st < minval || st > maxval) return true;
	else return false;
}

function checkLenAndVal(st, nd, rd, th) {
	switch(arguments.length){
		case 1:
			switch(options.value){
				case '0':
					if(checkLen(st, 3) || checkVal(st, 0, 255)) return true;
					else return false;
				break;

				case '1':
					if(checkLen(st, 20)) return true;
					else return false;
				break;
		
				case '2':
				case '6':
					if(checkHex0F(st)) return true;
					else return false;
				break;

				case '3':
				case '4':
					if(checkLen(st, 4) || isNaN(st)) return true;
					else if(checkVal(st, 0, 360)) return true;
					else if(checkVal(st, 0, 1) && st.indexOf(".") == 1) return true;
					else return false;
				break;

				case '5':
					if(checkLen(st, 4) || checkVal(st, 0 ,1) || isNaN(st)) return true;
					return false;
				break;
			}
		break;

		case 3:
			switch(options.value){
				case '0':
				if(checkLenAndVal(st) || checkLenAndVal(nd) || checkLenAndVal(rd)) return true;
				else return false;
				break;

				case '3':
				case '4':
				if(checkLenAndVal(st) || checkLenAndVal(nd) || checkLenAndVal(rd)) return true;
				else return false;
				break;
			}
		break;

		case 4:
			if(options.value == 5){
				if(checkLenAndVal(st) || checkLenAndVal(nd) || checkLenAndVal(rd) || checkLenAndVal(th)) return true;
				else return false;
			}
		break;
	}	
}

function checkNameExist(st){
	var arr_name = getColorArr("names");
	var exist = arr_name.indexOf(st);

	if(exist == -1) return true;
	else return false;
}

function checkHex0F(st){
	var hex_arr = getHexArr();
	var count=0;
	var i=0;

	for(i=0; i<st.length; i++){
		if(hex_arr.includes(st[i])){
			count++;
		}
	}
	if(count == 6) return true;
	else return false;
}

function createCanvas(where){
	var element = document.createElement("canvas");
	element.className = "color-palette";
	element.width = "284";
	element.height = "155";
	where.insertAdjacentElement('afterbegin', element);
	element = document.createElement("input");
	element.id = "clr1";
	element.type = "color";
	element.value = "#000020";
	document.querySelector(".color-palette").insertAdjacentElement('afterend', element);
	document.querySelector(".color-palette").style.backgroundColor = element.value;
}

function createLabelnInput(where, inner, placeholder){
	var element = document.createElement("label");
	element.className = "data__label";
	element.innerHTML = inner;
	where.appendChild(element);
	element = document.createElement("input");
	element.className = "data__input";
	element.type = "text";
	element.id = inner;
	element.placeholder = placeholder;
	where.appendChild(element);
}

function createResult(where, label, result){
	var element;
		element = document.createElement("div");
		element.className = "result__label";
		element.innerHTML = label;
		where.appendChild(element);
		element = document.createElement("div");
		element.className = "result__value";
		element.innerHTML = result;
		where.appendChild(element);				
}

function removeAllChildren(theParent){
    var rangeObj = new Range();
    rangeObj.selectNodeContents(theParent);
    rangeObj.deleteContents();
}

/////////////////////////////////////////////


function fromRGB(r,g,b){
	switch(options.value) {
		case '0':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"HEX", RGB2HEX(r,g,b));
			createResult(result,"HSL", RGB2HSL(r,g,b));
			createResult(result,"HSV", RGB2HSV(r,g,b));
			createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;

		case '1':
		createResult(result,"HEX", RGB2HEX(r,g,b));
		createResult(result,"HSL", RGB2HSL(r,g,b));
		createResult(result,"HSV", RGB2HSV(r,g,b));
		createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;

		case '2':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"RGB", "RGB("+r+","+g+","+b+")");
			createResult(result,"HSL", RGB2HSL(r,g,b));
			createResult(result,"HSV", RGB2HSV(r,g,b));
			createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;

		case '3':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"RGB", "RGB("+r+","+g+","+b+")");
			createResult(result,"HEX", RGB2HEX(r,g,b));
			createResult(result,"HSV", RGB2HSV(r,g,b));
			createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;

		case '4':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"RGB", "RGB("+r+","+g+","+b+")");
			createResult(result,"HEX", RGB2HEX(r,g,b));
			createResult(result,"HSL", RGB2HSL(r,g,b));
			createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;

		case '5':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"RGB", "RGB("+r+","+g+","+b+")");
			createResult(result,"HEX", RGB2HEX(r,g,b));
			createResult(result,"HSL", RGB2HSL(r,g,b));
			createResult(result,"HSV", RGB2HSV(r,g,b));
		break;

		case '6':
			createResult(result,"TEXT", RGB2TEXT(r,g,b));
			createResult(result,"RGB", "RGB("+r+","+g+","+b+")");
			createResult(result,"HEX", RGB2HEX(r,g,b));
			createResult(result,"HSL", RGB2HSL(r,g,b));
			createResult(result,"HSV", RGB2HSV(r,g,b));
			createResult(result,"CMYK", RGB2CMYK(r,g,b));
		break;
	}
}

function fromTEXT(name){
	var rgb = TEXT2RGB(name);
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];

	fromRGB(r,g,b);
}

function fromHEX(h){
	var rgb = HEX2RGB(h);
	rgb = rgb.replace("RGB(", "");
	rgb = rgb.replace(")", "");
	rgb = rgb.split(',');
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	fromRGB(r,g,b);
}

function fromHSL(h, s, l){
	var rgb = HSL2RGB(h,s,l);
	rgb = rgb.replace("RGB(", "");
	rgb = rgb.replace(")", "");
	rgb = rgb.split(',');
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	fromRGB(r,g,b);
}

function fromHSV(h, s, v){
	var rgb = HSV2RGB(h,s,v);
	rgb = rgb.replace("RGB(", "");
	rgb = rgb.replace(")", "");
	rgb = rgb.split(',');
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	fromRGB(r,g,b);
}

function fromCMYK(c, m, y, k){
	var rgb = CMYK2RGB(c,m,y,k);
	rgb = rgb.replace("RGB(", "");
	rgb = rgb.replace(")", "");
	rgb = rgb.split(',');
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	fromRGB(r,g,b);
}

//Coverters

function RGB2TEXT(r,g,b){
	var temp = RGB2HEX(r,g,b).replace('#', '');
	var nam_arr, hex_arr = [];
	var temp_name="color name";
	var temp_hex;

	nam_arr = getColorArr("names");
	hex_arr = getColorArr("hexs");

	temp_hex = hex_arr.indexOf(temp);

	if(temp_hex < 0){

		result.innerHTML="CANNOT FIND '"+ name.value + "' COLOUR IN DATABASE";
	}
	else{
	temp_name = nam_arr[temp_hex];
	return temp_name.toUpperCase();
	}
}

function RGB2HEX(r,g,b){
    var bin = r << 16 | g << 8 | b;

    return "#" + (function(h){
    	return new Array(7-h.length).join("0")+h })
    				(bin.toString(16).toUpperCase());

}
function RGB2HSL(r, g, b) {
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b),
        min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }
        h /= 6;
    }
    h = Math.round(h * 360);
    s = Math.round(s*100).toFixed(2);
    l = Math.round(l*100).toFixed(2);



    return "HSL(" + h + "°, " + (s/100).toFixed(2) + ", " + (l/100).toFixed(2) + ")";
}

function RGB2HSV(r, g, b) {
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b),
        min = Math.min(r, g, b);
    var h, s, v = max;

    var d = max - min;
    s = max == 0 ? 0 : d / max;

    if (max == min){
        h = 0;
    } 
    else{
        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }
        h /= 6;
    }

    h = Math.round(h * 360);
    s = Math.round(s * 100).toFixed(2);
    v = Math.round(v * 100).toFixed(2);


    return "HSV(" + h + "°, " + (s/100).toFixed(2) + ", " + (v/100).toFixed(2) + ")";
}
function RGB2CMYK(r, g, b) {
  var c, m, y, k;
  r = r / 255;
  g = g / 255;
  b = b / 255;
  max = Math.max(r, g, b);
  k = 1 - max;
  if (k == 1) {
    c = 0;
    m = 0;
    y = 0;
  } else {
    c = (1 - r - k) / (1 - k);
    m = (1 - g - k) / (1 - k);
    y = (1 - b - k) / (1 - k);
  }
  return "CMYK(" + c.toFixed(2).toString(16) + ", " + m.toFixed(2).toString(16) +
  		", " + y.toFixed(2).toString(16) + ", " + k.toFixed(2).toString(16)+ ")";
}

//////////////////////////////////////////////////

function HEX2RGB(hex){
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    return "RGB("+r+","+g+","+b+")";
}

function TEXT2RGB(name){
	var temp, temp_hex, index;
	var nam_arr, hex_arr, rgb = [];

	nam_arr = getColorArr("names");
	hex_arr = getColorArr("hexs");

	name = name.toString(16);

	index = nam_arr.indexOf(name);

	if(index < 0){
		if(name.length < 1)
		{
			name = "undefined";
		}
		result.innerHTML="CANNOT FIND '"+ name.value + "' COLOUR IN DATABASE";
	}
	else{
		temp_hex = hex_arr[index];
		temp = HEX2RGB(temp_hex);
	}
	createResult(result,"RGB", temp);
	temp = temp.replace("RGB(", "");
	temp = temp.replace(")", "");
	rgb=temp.split(',');
	return rgb;
}

function HSL2RGB(H, S, L){
	H = Math.round(H);
    var C = (1 - Math.abs((2 * L) - 1)) * S;
    var H_ = H / 60;
    var X = C * (1 - Math.abs((H_ % 2) - 1));
    var R1, G1, B1;

    if (H === undefined || isNaN(H) || H === null) {
        R1 = G1 = B1 = 0;
    }
    else {

        if (H_ >= 0 && H_ < 1) {
            R1 = C;
            G1 = X;
            B1 = 0;
        }
        else if (H_ >= 1 && H_ < 2) {
            R1 = X;
            G1 = C;
            B1 = 0;
        } else if (H_ >= 2 && H_ < 3) {
            R1 = 0;
            G1 = C;
            B1 = X;
        } else if (H_ >= 3 && H_ < 4) {
            R1 = 0;
            G1 = X;
            B1 = C;
        } else if (H_ >= 4 && H_ < 5) {
            R1 = X;
            G1 = 0;
            B1 = C;
        }
        else if (H_ >= 5 && H_ < 6) {
            R1 = C;
            G1 = 0;
            B1 = X;
        }
    }
    var m = L - (C / 2);

    var R, G, B;

    R = (R1 + m) * 255;
    G = (G1 + m) * 255;
    B = (B1 + m) * 255;

    R = Math.round(R);
    G = Math.round(G);
    B = Math.round(B);


  	return "RGB("+R+","+G+","+B+")";
}

function HSV2RGB(h, s, v) {   
	h = Math.round(h);                    
	var r, g, b;
	var i;
	var f, p, q, t;
	h = Math.max(0, Math.min(360, h));
	s = Math.max(0, Math.min(100, s));
	v = Math.max(0, Math.min(100, v)); 
 
	if(s == 0) {
		// Achromatic (grey)
		r = g = b = v;
		return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
	}
 
	h /= 60; 
	i = Math.floor(h);
	f = h - i; 
	p = v * (1 - s);
	q = v * (1 - s * f);
	t = v * (1 - s * (1 - f));
 
	switch(i) {
		case 0:
			r = v;
			g = t;
			b = p;
			break;
 
		case 1:
			r = q;
			g = v;
			b = p;
			break;
 
		case 2:
			r = p;
			g = v;
			b = t;
			break;
 
		case 3:
			r = p;
			g = q;
			b = v;
			break;
 
		case 4:
			r = t;
			g = p;
			b = v;
			break;
 
		default: 
			r = v;
			g = p;
			b = q;
	}
 
	r = Math.round(r * 255);
	g = Math.round(g * 255);
	b = Math.round(b * 255);

	return "RGB("+r+","+g+","+b+")";
}

function CMYK2RGB(c, m, y, k){
    var r = Math.round(255 * (1-c) * (1-k));
    var g = Math.round(255 * (1-m) * (1-k));
    var b = Math.round(255 * (1-y) * (1-k));

  return "RGB("+r+","+g+","+b+")";
}

//ARR GENERATORS
function getColorArr(x) {
  if (x == "names") {return[
  				'ALICEBLUE','ANTIQUEWHITE','AQUA','AQUAMARINE','AZURE','BEIGE',
  				'BISQUE','BLACK','BLANCHEDALMOND','BLUE','BLUEVIOLET','BROWN','BURLYWOOD','CADETBLUE',
  				'CHARTREUSE','CHOCOLATE','CORAL','CORNFLOWERBLUE','CORNSILK','CRIMSON','CYAN',
  				'DARKBLUE','DARKCYAN','DARKGOLDENROD','DARKGRAY','DARKGREY','DARKGREEN','DARKKHAKI',
  				'DARKMAGENTA','DARKOLIVEGREEN','DARKORANGE','DARKORCHID','DARKRED','DARKSALMON',
  				'DARKSEAGREEN','DARKSLATEBLUE','DARKSLATEGRAY','DARKSLATEGREY','DARKTURQUOISE',
  				'DARKVIOLET','DEEPPINK','DEEPSKYBLUE','DIMGRAY','DIMGREY','DODGERBLUE','FIREBRICK',
  				'FLORALWHITE','FORESTGREEN','FUCHSIA','GAINSBORO','GHOSTWHITE','GOLD','GOLDENROD',
  				'GRAY','GREY','GREEN','GREENYELLOW','HONEYDEW','HOTPINK','INDIANRED','INDIGO','IVORY',
  				'KHAKI','LAVENDER','LAVENDERBLUSH','LAWNGREEN','LEMONCHIFFON','LIGHTBLUE','LIGHTCORAL',
  				'LIGHTCYAN','LIGHTGOLDENRODYELLOW','LIGHTGRAY','LIGHTGREY','LIGHTGREEN','LIGHTPINK',
  				'LIGHTSALMON','LIGHTSEAGREEN','LIGHTSKYBLUE','LIGHTSLATEGRAY','LIGHTSLATEGREY',
  				'LIGHTSTEELBLUE','LIGHTYELLOW','LIME','LIMEGREEN','LINEN','MAGENTA','MAROON',
  				'MEDIUMAQUAMARINE','MEDIUMBLUE','MEDIUMORCHID','MEDIUMPURPLE','MEDIUMSEAGREEN',
  				'MEDIUMSLATEBLUE','MEDIUMSPRINGGREEN','MEDIUMTURQUOISE','MEDIUMVIOLETRED','MIDNIGHTBLUE',
  				'MINTCREAM','MISTYROSE','MOCCASIN','NAVAJOWHITE','NAVY','OLDLACE','OLIVE','OLIVEDRAB','ORANGE',
  				'ORANGERED','ORCHID','PALEGOLDENROD','PALEGREEN','PALETURQUOISE','PALEVIOLETRED','PAPAYAWHIP',
  				'PEACHPUFF','PERU','PINK','PLUM','POWDERBLUE','PURPLE','REBECCAPURPLE','RED','ROSYBROWN',
  				'ROYALBLUE','SADDLEBROWN','SALMON','SANDYBROWN','SEAGREEN','SEASHELL','SIENNA','SILVER',
  				'SKYBLUE','SLATEBLUE','SLATEGRAY','SLATEGREY','SNOW','SPRINGGREEN','STEELBLUE','TAN','TEAL',
  				'THISTLE','TOMATO','TURQUOISE','VIOLET','WHEAT','WHITE','WHITESMOKE','YELLOW','YELLOWGREEN'];}
  
  if (x == "hexs") {return ['F0F8FF','FAEBD7','00FFFF','7FFFD4','F0FFFF','F5F5DC','FFE4C4','000000','FFEBCD',
  				'0000FF','8A2BE2','A52A2A','DEB887','5F9EA0','7FFF00','D2691E','FF7F50','6495ED','FFF8DC',
  			'DC143C','00FFFF','00008B','008B8B','B8860B','A9A9A9','A9A9A9','006400','DBD76B','8B008B','556B2F',
  			'FF8C00','9932CC','8B0000','E9967A','8FBC8F','483D8B','2F4F4F','2F4F4F','00CED1','9400D3','FF1493',
  			'00BFFF','696969','696969','1E90FF','B22222','FFFAF0','228B22','FF00FF','DCDCDC','F8F8FF','FFD700',
  			'DAA520','808080','808080','008000','ADFF2F','F0FFF0','FF69B4','CD5C5C','4B0082','FFFFF0','F0E68C',
  			'E6E6FA','FFF0F5','7CFC00','FFFACD','ADD8E6','F08080','E0FFFF','FAFAD2','D3D3D3','D3D3D3','90EE90',
  			'FFB6C1','FFA07A','20B2AA','87CEFA','778899','778899','B0C4DE','FFFFE0','00FF00','32CD32','FAF0E6',
  			'FF00FF','800000','66CDAA','0000CD','BA55D3','9370DB','3CB371','7B68EE','00FA9A','48D1CC','C71585',
  			'191970','F5FFFA','FFE4E1','FFE4B5','FFDEAD','000080','FDF5E6','808000','6B8E23','FFA500','FF4500',
  			'DA70D6','EEE8AA','98FB98','AFEEEE','DB7093','FFEFD5','FFDAB9','CD853F','FFC0CB','DDA0DD','B0E0E6',
  			'800080','663399','FF0000','BC8F8F','4169E1','8B4513','FA8072','F4A460','2E8B57','FFF5EE','A0522D',
  			'C0C0C0','87CEEB','6A5ACD','708090','708090','FFFAFA','00FF7F','4682B4','D2B48C','008080','D8BFD8',
  			'FF6347','40E0D0','EE82EE','F5DEB3','FFFFFF','F5F5F5','FFFF00','9ACD32']; }
}

function getHexArr() {

	return ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
}
