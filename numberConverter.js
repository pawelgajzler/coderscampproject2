//validation functions
function isHex(num){
  numbers = /^[0-9a-fA-F]+$/;
  return numbers.test(num);
}

function isDec(num){
  numbers = /^[0-9]+$/;
  return numbers.test(num);
}

function isOct(num){
  numbers = /^[0-7]+$/;
  return numbers.test(num);
}

function isBin(num){
  numbers = /^[0-1]+$/;
  return numbers.test(num);
}

//hex functions
const hexArray=['a','b','c','d','e','f']

function hexToNum(num){
    num.toLowerCase();
    let y= [];
    for (let i=0; i<num.length; i++){
      (num[i]<10)? y[i]=parseInt(num[i]) : y.push(hexArray.indexOf(num[i])+10);}
  return y;
}

function numToHex(num){
    let y = [];
    for (let i=0; i<num.length; i++){
      (num[i]<10)? y[i]=String(num[i]) : y.push(hexArray[num[i]-10]);}
  return y;
}

//conversions to dec

function binToDec(num){
    if(isBin(num)==false) return 'Wprowadzona liczba nie jest binarną';
    else {
      let y = String(num);
      y=y.split('');
      y.reverse();
      for(let i=0; i<y.length;i++) y[i]*=Math.pow(2,i);
      let total=y.reduce((accumulator, currentValue) => accumulator + currentValue);
      return String(total);
    }
}

function octToDec(num){
    if (isOct(num)==false) return 'Wprowadzona liczba nie jest liczbą ósemkową';
    else {
      let y = String(num);
      y=y.split('');
      y.reverse();
      for(let i=0; i<y.length;i++) y[i]*=Math.pow(8,i);
      let total=y.reduce((accumulator, currentValue) => accumulator + currentValue);
      return String(total);
    }
}

function hexToDec(num){
    if (isHex(num)==false) return 'Wprowadzona liczba nie jest liczbą szesnastkową';
    else {
     let y=hexToNum(num);
     y.reverse();
     for(let i=0; i<y.length;i++) y[i]*=Math.pow(16,i);
     let total=y.reduce((accumulator, currentValue) => accumulator + currentValue);
     return String(total);
    }
  }

// From Dec functions

function decToBin(num){
    let total=[];
    if(isDec(num)==false) return 'Wprowadzona liczba nie jest liczbą dziesiętną';
    else {
    do {
      total.unshift(num % 2);
      num=(num-(num%2))/2;
    } while(num>0);}
    return String(total.join(''));
}

function decToOct(num){
    let total=[];
    if(isDec(num)==false) return 'Wprowadzona liczba nie jest liczbą dziesiętną';
    else {
    do {
      total.unshift(num % 8);
      num=(num-(num%8))/8;
    } while(num>0);}
    return String(total.join(''));
}

function decToDec(num){ return isDec(num)? num: 'Wprowadzona liczba nie jest liczbą dziesiętną'}

function decToHex(num){
    let total=[];
    if(isDec(num)==false) return 'Wprowadzona liczba nie jest liczbą dziesiętną';
    else {
    do {
      total.unshift(num % 16);
      num=(num-(num%16))/16;
    } while(num>0);}
    return numToHex(total).join('').toUpperCase();
}

//funkcje hexTo
function hexToBin(num) { return isHex(num)? decToBin(hexToDec(num)) : 'Wprowadzona liczba nie jest liczbą szesnastkową';}

function hexToOct(num) { return isHex(num)? decToOct(hexToDec(num)) : 'Wprowadzona liczba nie jest liczbą szesnastkową';}

function hexToHex(num) { return isHex(num)? num.toUpperCase() : 'Wprowadzona liczba nie jest liczbą szesnastkową';}

//funkcje octTo
function octToHex(num) { return isOct(num)? decToHex(octToDec(num)) : 'Wprowadzona liczba nie jest liczbą ósemkową';}

function octToOct(num) { return isOct(num)? num : 'Wprowadzona liczba nie jest liczbą ósemkową';}

function octToBin(num) { return isOct(num)? decToBin(octToDec(num)) : 'Wprowadzona liczba nie jest liczbą ósemkową';}

//funkcje binTo
function binToBin(num) { return isBin(num)? num : 'Wprowadzona liczba nie jest binarną';}

function binToOct(num) { return isBin(num)? decToOct(binToDec(num)) : 'Wprowadzona liczba nie jest binarną';}

function binToHex(num) { return isBin(num)? decToHex(binToDec(num)) : 'Wprowadzona liczba nie jest binarną';}